const sql = require('./db')

let create = {}
create.createDb = () => {
  return new Promise((resolve, reject) => {
    sql.query(`CREATE DATABASE lowRiders`, (err, result) => {
      if (err) {
        return reject(err)
      }
      return resolve(result)
    })
  })

}

create.createtables = async () => {

  try {

    const c4 = await new Promise((resolve, reject) => {
      sql.query(`CREATE TABLE medical_histories(
              id BIGINT(20) AUTO_INCREMENT PRIMARY KEY UNIQUE,
              illness MEDIUMTEXT,
              history LONGTEXT
            )`, (err, res) => {
        if (err) {
          return reject(err)
        }
        return resolve(res)
      })
    }).catch(err => console.error(err))

    const c3 = await new Promise((resolve, reject) => {
      sql.query(`CREATE TABLE traits(
              id BIGINT(20) AUTO_INCREMENT PRIMARY KEY UNIQUE,
              cat_friendly TINYINT(1),
              kid_friendly TINYINT(1),
              dog_friendly TINYINT(1),
              touch_sensistive TINYINT(1)
            )`, (err, res) => {
        if (err) {
          return reject(err)
        }
        return resolve(res)
      })
    }).catch(err => console.error(err))

    const c2 = await new Promise((resolve, reject) => {
      sql.query(`CREATE TABLE drives(
               id BIGINT(20) AUTO_INCREMENT PRIMARY KEY UNIQUE,
               prey TINYINT(1),
               toy TINYINT(1),
               food TINYINT(1)
             )`, (err, res) => {
        if (err) {
          return reject(err)
        }
        return resolve(res)
      })
    }).catch(err => console.error(err))

    const c1 = await new Promise((resolve, reject) => {
      sql.query(`CREATE TABLE dogs(
        id BIGINT(20) AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(100),
        sex ENUM('male', 'female', 'unknown'),
        arrival_date DATETIME,
        story VARCHAR(255),
        breed VARCHAR(100),
        status ENUM('new arrival', 'adoption pending', 'adopted', 'foster'),
        location TINYTEXT,
        medical_history_id  BIGINT(20),
        FOREIGN KEY(medical_history_id) REFERENCES medical_histories(id),
        trait_id BIGINT(20),
        FOREIGN KEY(trait_id) REFERENCES traits(id),
        weight INT,
        age INT,
        drive_id BIGINT(20),
        FOREIGN KEY(drive_id) REFERENCES drives(id)
      )`, (err, res) => {
        if (err) {
          return reject(err)
        }
        return resolve(res)
      })
    }).catch(err => console.error(err))

    const ref = await new Promise((resolve, reject) => {
      sql.query(`CREATE TABLE refs(
        id BIGINT(20) AUTO_INCREMENT PRIMARY KEY UNIQUE,
        name VARCHAR(100),
        phone BIGINT(20),
        email VARCHAR(75),
        name_2 VARCHAR(20),
        phone_2 BIGINT(20),
        email_2 VARCHAR(75)
      )`, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    }).catch(err => console.error(err))

    const a1 = await new Promise((resolve, reject) => {
      sql.query(`CREATE TABLE applications(
        id BIGINT(20) AUTO_INCREMENT PRIMARY KEY UNIQUE,
        date DATETIME,
        primary_caretaker VARCHAR(100),
        age INT,
        address VARCHAR(255),
        email VARCHAR(75),
        phone BIGINT(20),
        co_applicant VARCHAR(100),
        rescued_before TINYINT(1),
        rescue_name VARCHAR(100),
        decline_history TINYINT(1),
        decline_reason VARCHAR(75),
        decline_explanation LONGTEXT,
        housing MEDIUMTEXT,
        own_lease ENUM('Own', 'lease', 'rent'),
        landlord_permission TINYINT(1),
        landlord_name VARCHAR(100),
        landlord_phone BIGINT(20),
        hoa_restrictions TINYINT(1),
        hoa_restrictions_description MEDIUMTEXT,
        fenced_yard TINYINT(1),
        fence_type VARCHAR(75),
        num_residents INT,
        num_children INT,
        household_consensus TINYINT(1),
        vet_name VARCHAR(100),
        vet_phone BIGINT(20),
        other_pets TINYINT(1),
        other_pets_description MEDIUMTEXT,
        previous_pets TINYINT(1),
        previous_pets_description MEDIUMTEXT,
        other_pets_vaccination_history MEDIUMTEXT,
        other_pets_spay_neuter_history MEDIUMTEXT,
        explanation MEDIUMTEXT,
        daily_alone_time INT,
        alone_time_setting TINYTEXT,
        bed_location VARCHAR(255),
        relocation_contingency MEDIUMTEXT,
        boarding_plan MEDIUMTEXT,
        job_contingency TINYINT(1),
        house_training TINYINT(1),
        introduction_plan MEDIUMTEXT,
        behavioral_contingencies MEDIUMTEXT,
        puppy_training TINYINT(1),
        financial_ability BIGINT(20),
        experience_level INT,
        runaway_contingency TINYINT(1),
        food MEDIUMTEXT,
        exercise TINYINT(1),
        citation_history TINYINT(1),
        changes MEDIUMTEXT,
        return_reasons MEDIUMTEXT,
        timeline VARCHAR(255),
        how_did_you_hear_about_us VARCHAR(100),
        additional_info MEDIUMTEXT,
        ref_id BIGINT(20),
        FOREIGN KEY(ref_id) REFERENCES refs(id)
      )`, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    }).catch((err) => console.error(err))

    const a2 = await new Promise((resolve, reject) => {
      sql.query(`CREATE TABLE dogs_applicaitons(
        dog_id BIGINT(20),
        applications_id BIGINT(20)
      )`, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    }).catch((err) => console.error(err))

  } catch (err) {
    console.error(err)

  } finally {
    console.log('tables created');
  }
}

create.seedTables = async () => {
  try {
    const medHisdat = await new Promise((resolve, reject) => {
      sql.query(`INSERT INTO medical_histories(illness, history) values ( 'none', 'A healthy pup Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum' ),
        ( 'rabies', 'we found this sweetie all alone but she is much better now Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum' ),
        ( 'Alopecia', 'Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum' ),
        ( 'none', 'Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum' ),
        ( 'diabetes', 'Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum' )`, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    }).catch(err => console.error(err))

    const drivedat = await new Promise((resolve, reject) => {
      sql.query(`INSERT INTO drives(prey, toy, food) values ( 0, 1, 1 ),
        ( 1, 0, 1 ),
        ( 0, 0, 1 ),
        ( 1, 1, 0 ),
        ( 0, 1, 1 )`, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    }).catch(err => console.error(err))

    const traitdat = await new Promise((resolve, reject) => {
      sql.query(`INSERT INTO traits(cat_friendly, kid_friendly, dog_friendly, touch_sensistive) values ( 0, 1, 1, 0 ),
        ( 1, 1, 0, 1 ),
        ( 0, 1, 0, 1 ),
        ( 0, 0, 1, 1 ),
        ( 0, 1, 1, 0 )`, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    }).catch(err => console.error(err))

    const dogdat = await new Promise((resolve, reject) => {
      sql.query(`INSERT INTO dogs(name, sex, arrival_date, story, breed, status, location, medical_history_id, trait_id, weight, age, drive_id) values ( 'Roxy', 'female', '2008-08-13', 'Lorem ipsum dolor t nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum', 'Daschund', 'foster', 'Chicago', 1, 1, '120', '12', 1 ),
        ( 'Max', 'male', '2015-05-02', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et id est laborum', 'Chihuahua', 'new arrival', 'Chicago', 2, 2, '110', '4', 2 ),
        ( 'Jazzy', 'female', '2012-07-31', 'Lorem ipsum eprehenderit in voluptate t nulla pariatur Excepteur sint occaecat ent sunt in culpa qui officia deserunt mollit anim id est laborum', 'Corgi', 'adopted', 'Denver', 3, 3, '50', '7', 3),
        ( 'Bruce', 'male', '2014-01-04', 'Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 'Jack Russel Terrier', 'adoption pending', 'Dale', 4, 4, '36', '5', 4 ),
        ( 'Scarlett', 'female', '2015-10-01', 'Lorem ipsum dolor sit amet consectetur adipisicing elit sfdeasmds fdsm', 'Beagle', 'new arrival', 'Bern', 5, 5, '35', '4', 5 )`, (err, result) => {
        if (err) {
          return reject(err)
        }
        return resolve(result)
      })
    }).catch(err => console.error(err))

  } catch (err) {
    console.error(err)

  } finally {
    console.log('seed completed');
  }
}

module.exports = create