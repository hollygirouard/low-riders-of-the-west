const sql = require('./db')

let testDog = {}

// dog index route

testDog.all = () => {

  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM dogs`, (err, res) => {
      if (err) {
        return reject(err)
      }
      return resolve(res)
    })
  })
}

// dog find by id

testDog.findOne = (id) => {

  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM dogs WHERE id = ${id} `, (err, res) => {
      if (err) {
        return reject(err)
      }
      return resolve(res)
    })
  })
}

// add to medical_history table

testDog.createDog = (body) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO medical_histories(illness, history) values (${body.medical_history.illness}, ${body.medical_history.history})`, (err, res) => {
      if (err) {
        return reject(err)
      }
      return resolve(res)
    })
  })
}

module.exports = testDog