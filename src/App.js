import React, { useState } from 'react'
import {
  Navbar,
  NavbarBrand,
  NavItem,
  NavLink,
} from 'reactstrap'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import Routes from './Routes'
import { navItems } from './helpers/constants'
import './styles/app.scss'
import { NewLogo } from './assets'

library.add(faCheck)

const App = () => {
  return (
    <div className="app">
      <Navbar className="main-navigation" light expand="md">
        <NavbarBrand href="/">
          <img src={ NewLogo } alt="home" height="100px" />
        </NavbarBrand>
        <div className="justify-nav-links">
          { navItems.map(nav => (
            <NavItem key={ nav.href }>
              <NavLink
                href={ nav.href }
                alt={ nav.alt }
                key={ nav.title }
              >
                { nav.title }
              </NavLink>
            </NavItem>
          ))
          }
        </div>
      </Navbar>
      <div className="screen">
        <Routes />
      </div>
      <div className="grey-container">
        <div className="top-grey" />
      </div>
    </div>
  )
}

export default App
