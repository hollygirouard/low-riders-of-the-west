const colors = {
  brandGreen: '#489B7C',
  brandCoral: '#F66B69',
  brandDarkBlue: '#003040',
  brandExtraLightGray: '#e8e8e8',
  brandLightGray: '#979797',
  brandDarkGray: '#434343',
  brandMediumGray: '#666666',
  black: '#000', // Black
  white: '#FFF', // White
}

const navItems = [
  {
    title: 'Ways to Help',
    href: '/ways-to-help',
    alt: 'ways to help',
  },
  {
    title: 'Adopt a Dog',
    href: '/adopt',
    alt: 'adopt',
  },
  {
    title: 'Foster a Dog',
    href: '/foster',
    alt: 'foster',
  },
  {
    title: 'Events',
    href: '/events',
    alt: 'events',
  },
  {
    title: 'Gallery',
    href: '/gallery',
    alt: 'gallery',
  },
]

const homepageAdoptableLimit = 3

export { navItems, colors, homepageAdoptableLimit }
