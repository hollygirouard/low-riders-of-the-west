import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { colors } from '../helpers/constants'
import { DefaultButton } from '../components/formComponents'
import ReusableCarousel from '../components/ReusableCarousel'

const SingleDog = ({
  dogName, dogType, dogGender, dogAge, dogLikes, dogHealth, dogFee,
}) => (
  <div className="solo-doggo">
    <Container>
      <Row>
        <Col sm={ 10 } md={ 6 } lg={ 6 } className="left-solo">
          <ReusableCarousel />
          <Col className="traits">
            <div className="trait-title">
              <FontAwesomeIcon icon="check" color={ colors.brandGreen } />{' '} Likes
            </div>
            { dogLikes || 'Long walks, great with other dogs and kids'}
          </Col>
          <Col className="traits">
            <div className="trait-title">
              <FontAwesomeIcon icon="check" color={ colors.brandGreen } />{' '} Health
            </div>
            { dogHealth || 'Spayed, up to date on vaccinations, micro-chipped, example of multi line entry' }
          </Col>
          <Col className="traits">
            <div className="trait-title">
              <FontAwesomeIcon icon="check" color={ colors.brandGreen } />{' '} Adoption Fee
            </div>
            { dogFee || '$325' }
          </Col>
        </Col>
        <Col sm={ 10 } md={ 6 } lg={ 6 }>
          <h1>{ dogName || 'Taco' }</h1>
          <div className="doggo-details">
            { dogAge || 'Adult' } • { dogGender || 'Male' } • { dogType || 'Mix' }
          </div>
          <div className="dog-info">
            This is the description of the dog. Lorem ipsum dolor sit amet, consectetur
             adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            <br /><br />
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor
               sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur
                adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                 Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            <br /><br />
                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem
                  ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor
                   sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur
                     adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum
                       dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur
                         adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </div>
          <div className="center">
            <DefaultButton styling="coral-button" title="Apply to Adopt Taco" />
          </div>
        </Col>
      </Row>
    </Container>
  </div>
)

export default SingleDog
