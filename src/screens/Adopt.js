import React from 'react'
import { Container } from 'reactstrap'
import { AdoptableDogs } from '../components'

const Adopt = () => (
  <div className="adopt-page">
    <Container>
      <AdoptableDogs />
    </Container>
  </div>
)

export default Adopt
