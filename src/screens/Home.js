/* eslint-disable max-len */
import React from 'react'
import { TopHomeContainer, AboutUs, AdoptableDogs, Partners, ContactUs } from '../components'
import { homepageAdoptableLimit } from '../helpers/constants'

const Home = () => (
  <div>
    <TopHomeContainer />
    <AboutUs />
    <AdoptableDogs limit={ homepageAdoptableLimit } />
    <Partners />
    <ContactUs />
  </div>
)

export default Home
