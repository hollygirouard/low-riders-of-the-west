import React from 'react'
import { Container } from 'reactstrap'

const WaysToHelp = () => (
  <div>
    <Container><h1>Ways To Help</h1></Container>
    <Container>
      <h3>To Donate Please Click Below</h3>
      <button>Donate</button>
      <p>You will be redirected to our secure online server. We thank you for your support!</p>
    </Container>
    <Container>
      <img src="" alt="Amazon Smile placeholder" />
    </Container>
    <Container>
      <p>If you would like to donate items please click below</p>
      <button>Amazon Wishlist</button>
    </Container>
  </div>
)

export default WaysToHelp
