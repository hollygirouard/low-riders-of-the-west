import React from 'react'

const fosterInfoText = [
  'Low Riders of the West desperately needs foster homes for dogs and puppies.  Fostering saves lives, one dog at a time.',

  'Fostering is a short-term and temporary commitment, and your help gives these wonderful dogs a much better chance of getting adopted and finding their forever homes.  The love, affection, socialization, training, exercise and playtime you provide will make a difference that will affect your foster pet for the rest of his or her life.',

  'By opening up your home to foster pets, you’re not only helping to save lives, you’re providing the individual attention and love these dogs desperately need.',

  'Once you have completed your foster application online, our foster coordinator will get in touch with you to schedule a home check.',

  'Through fostering, we can work together to Save Them All.',
]

const Foster = () => (
  <div>
    { fosterInfoText.map((paragraph, index) => <p key={ index }>{ paragraph }</p>) }
  </div>
)

export default Foster
