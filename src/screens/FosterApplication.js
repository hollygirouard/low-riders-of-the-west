import React, { useState } from 'react'
import { Container, Form, Input, Label, Row } from 'reactstrap'
import { DefaultButton, TextInput } from '../components/formComponents'

const FosterApplication = () => {
  const dogTypes = [
    {
      name: 'Pregnant Dog-whelping puppies',
      checked: false,
    },
    {
      name: 'Nursing mom with puppies',
      checked: false,
    },
    {
      name: 'Orphaned puppies requiring bottle feeding (24 hour care)',
      checked: false,
    },
    {
      name: 'Seniors',
      checked: false,
    },
    {
      name: 'Females',
      checked: false,
    },
    {
      name: 'Males',
      checked: false,
    },
    {
      name: 'Fearful',
      checked: false,
    },
    {
      name: 'Behavioral problems',
      checked: false,
    },
    {
      name: 'Health recovery or hospice',
      checked: false,
    },
    {
      name: 'Need socialization',
      checked: false,
    },
  ]
  const references = [
    {
      name: '',
      phone: '',
    },
    {
      name: '',
      phone: '',
    },
    {
      name: '',
      phone: '',
    },
  ]
  const [ user, setUser ] = useState({
    address1: '',
    address2: '',
    agreementSigned: false,
    animalsSpayedNeutered: 'No',
    canAdministerMedication: 'No',
    canBringToAdoptionEvents: 'No',
    canFosterRepresentativeVisit: 'No',
    canHostPotentialAdopters: 'No',
    canProvideFood: 'No',
    canProvideVaccinationProof: 'No',
    canTransportToNeuterSpay: 'No',
    canTransportToVet: 'No',
    caretaker: '',
    childrenAges: [ '' ],
    city: '',
    country: '',
    currentlyHasPets: 'No',
    dogDoor: 'No',
    dogTypes,
    dwelling: 'Condo',
    email: '',
    fenceHeight: '',
    fenceType: '',
    fencedYard: 'No',
    fosterDayLocation: '',
    fosterExercise: '',
    fosterNightLocation: '',
    fosterLeftAloneHrs: '',
    fosterLeftAloneLocation: '',
    fosterQuestionsConcerns: '',
    fosterReason: '',
    hadPetsBefore: 'No',
    hasChildren: 'No',
    haveWarmDryVentilated: 'No',
    landlordName: '',
    landlordPermission: 'No',
    landlordPhone: '',
    name: '',
    ownKennel: '',
    petBreeds: [ '' ],
    petNames: [ '' ],
    petsCurrentOnVaccinations: 'No',
    phone: '',
    references,
    rentOwn: 'Rent',
    state: '',
    trainingExperience: '',
    veterinarianName: '',
    veterinarianPhone: '',
    visitingChildrenUnder5: 'No',
    zip: '',
  })
  const [ application, setApplication ] = useState({
    date: '',
  })
  return (
    <div>
      <Container>
        <h1>Foster Application</h1>
        <p>
          Please provide the following information to help us make the best match between a foster home and a foster animal
        </p>
        <Form onSubmit={ (e) => {
            e.preventDefault()
            console.log(user, application)
          } }
        >
          <h1>CONTACT AND PERSONAL INFORMATION</h1>
          <p>* Indicates required field</p>
          <Row>
            <TextInput
              text="Email - We will send you an application code so you can return to finish your application later"
              placeholder="my@email.com"
              id="email"
              value={ user.email }
              handleChange={ e => setUser( { ...user, email: e.target.value } ) }
              type="email"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Date"
              placeholder="mm/dd/yyyy"
              id="date"
              value={ application.date }
              handleChange={ e => setApplication( { ...application, date: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Name"
              placeholder="Full Name"
              id="name"
              value={ user.name }
              handleChange={ e => setUser( { ...user, name: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Address Line 1"
              placeholder="123 Main Street"
              id="address1"
              value={ user.address1 }
              handleChange={ e => setUser( { ...user, address1: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Address Line 2"
              placeholder="Apt 2B"
              id="address2"
              value={ user.address2 }
              handleChange={ e => setUser( { ...user, address2: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
            />
          </Row>
          <Row>
            <TextInput
              text="City"
              placeholder="Newtown"
              id="city"
              value={ user.city }
              handleChange={ e => setUser( { ...user, city: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="State"
              placeholder="CO"
              id="state"
              value={ user.state }
              handleChange={ e => setUser( { ...user, state: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Zip Code"
              placeholder="12345"
              id="zip"
              value={ user.zip }
              handleChange={ e => setUser( { ...user, zip: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Country"
              placeholder="United States"
              id="country"
              value={ user.country }
              handleChange={ e => setUser( { ...user, country: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Phone Number"
              placeholder="123-456-7890"
              id="phone"
              value={ user.phone }
              handleChange={ e => setUser( { ...user, phone: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Label className="input-label">
            I live in a <span className="required-star">*</span>
          </Label>
          <Input
            type="select"
            name="dwelling"
            id="dwelling"
            onChange={ e => setUser( { ...user, dwelling: e.target.value } ) }
          >
            <option>Condo</option>
            <option>Townhouse</option>
            <option>Apartment</option>
            <option>House</option>
          </Input>
          <Label className="input-label">
            I rent or own my living space <span className="required-star">*</span>
          </Label>
          <Input
            type="select"
            name="rentOwn"
            id="rentOwn"
            onChange={ e => setUser( { ...user, rentOwn: e.target.value } ) }
          >
            <option>Own</option>
            <option>Rent</option>
          </Input>
          { user.rentOwn === 'Rent' &&
            <div>
              <p>Please provide the landlord's contact information:</p>
              <Row>
                <TextInput
                  text="Name"
                  placeholder="Alex Landlord"
                  id="landlordName"
                  value={ user.landlordName }
                  handleChange={ e => setUser( { ...user, landlordName: e.target.value } ) }
                  type="text"
                  lgColSize={ 8 }
                  mdColSize={ 8 }
                  smColSize={ 12 }
                  xsColSize={ 12 }
                  required
                />
              </Row>
              <Row>
                <TextInput
                  text="Phone"
                  placeholder="123-456-7890"
                  id="landlordPhone"
                  value={ user.landlordPhone }
                  handleChange={ e => setUser( { ...user, landlordPhone: e.target.value } ) }
                  type="text"
                  lgColSize={ 8 }
                  mdColSize={ 8 }
                  smColSize={ 12 }
                  xsColSize={ 12 }
                  required
                />
              </Row>
              <p>Has your landlord listed above given permission to have a dog in your home?</p>
              <Input
                type="select"
                name="landlordPermission"
                id="landlordPermission"
                onChange={ e =>
                  setUser( { ...user, landlordPermission: e.target.value } )
                }
              >
                <option>No</option>
                <option>Yes</option>
              </Input>
              <p>If renting it is your responsibility to verify any breed restrictions with your landlord prior to agreeing to foster.</p>
            </div>
          }
          <p>
            Do any children live in your household?
            <span className="required-star">*</span>
          </p>
          <Input
            type="select"
            name="children"
            id="children"
            onChange={ e => setUser( { ...user, hasChildren: e.target.value } ) }
          >
            <option>No</option>
            <option>Yes</option>
          </Input>
          { user.hasChildren === 'Yes' &&
            <div>
              <Row>
                <p>
                  What age are the children in the house (in years)?
                  <span className="required-star">*</span>
                </p>
              </Row>
              <Row>
                { user.childrenAges.map((age, index) => (
                  <TextInput
                    placeholder="6"
                    value={ age }
                    key={ index }
                    handleChange={ (e) => {
                      user.childrenAges[ index ] = e.target.value
                      setUser( {
                        ...user,
                        childrenAges: user.childrenAges,
                        childrenUnder5: user.childrenAges.find(num => num < 5),
                      } )
                    } }
                    type="number"
                    lgColSize={ 2 }
                    mdColSize={ 2 }
                    smColSize={ 4 }
                    xsColSize={ 4 }
                  />
                ))}
                <DefaultButton
                  className=""
                  handleClick={ () => setUser({ ...user, childrenAges: [ ...user.childrenAges, '' ] }) }
                  title="+"
                />
              </Row>
            </div>
          }
          <p>
            Do any children under 5 visit your home?
            <span className="required-star">*</span>
          </p>
          <Input
            type="select"
            name="visitingChildrenUnder5"
            id="visitingChildrenUnder5"
            onChange={ e => setUser( { ...user, visitingChildrenUnder5: e.target.value } ) }
          >
            <option>No</option>
            <option>Yes</option>
          </Input>
          <p>
            Who will take care of your animals if you are out of town?
            <span className="required-star">*</span>
          </p>
          <Row>
            <TextInput
              placeholder="Full Name"
              id="caretaker"
              value={ user.caretaker }
              handleChange={ e => setUser( { ...user, caretaker: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <h1>PERSONAL PET INFORMATION</h1>
          <Row>
            <p>
              Have you had dogs in the past?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="hadPetsBefore"
              id="hadPetsBefore"
              onChange={ e => setUser( {
                ...user,
                hadPetsBefore: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Do you currently have pets?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="currentlyHasPets"
              id="currentlyHasPets"
              onChange={ e => setUser( {
                ...user,
                currentlyHasPets: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          { user.currentlyHasPets === 'Yes' && (
            <div>
              <Row>
                <p>
                  Are your pets current on vaccinations?
                  <span className="required-star">*</span>
                </p>
                <Input
                  type="select"
                  name="petsCurrentOnVaccinations"
                  id="petsCurrentOnVaccinations"
                  onChange={ e => setUser( {
                    ...user, petsCurrentOnVaccinations: e.target.value,
                  } ) }
                >
                  <option>No</option>
                  <option>Yes</option>
                </Input>
              </Row>
              <Row>
                <p>
                  Can you provide proof of vaccination?
                  <span className="required-star">*</span>
                </p>
                <Input
                  type="select"
                  name="canProvideVaccinationProof"
                  id="canProvideVaccinationProof"
                  onChange={ e => setUser( {
                    ...user,
                    canProvideVaccinationProof: e.target.value,
                  } ) }
                >
                  <option>No</option>
                  <option>Yes</option>
                </Input>
              </Row>
              <Row>
                <p>
                  Are your animals spayed/neutered?
                  <span className="required-star">*</span>
                </p>
                <Input
                  type="select"
                  name="animalsSpayedNeutered"
                  id="animalsSpayedNeutered"
                  onChange={ e => setUser( {
                    ...user,
                    animalsSpayedNeutered: e.target.value,
                  } ) }
                >
                  <option>No</option>
                  <option>Yes</option>
                </Input>
              </Row>
              <Row>
                <p>
                  List names of your pets:
                  <span className="required-star">*</span>
                </p>
              </Row>
              <Row>
                { user.petNames.map((name, index) => (
                  <TextInput
                    placeholder="Fido"
                    value={ name }
                    key={ index }
                    handleChange={ (e) => {
                      user.petNames[ index ] = e.target.value
                      setUser( {
                        ...user,
                        petNames: user.petNames,
                      } )
                    } }
                    type="text"
                    lgColSize={ 2 }
                    mdColSize={ 2 }
                    smColSize={ 4 }
                    xsColSize={ 4 }
                  />
                ))}
                <DefaultButton
                  className=""
                  handleClick={ () => setUser({ ...user, petNames: [ ...user.petNames, '' ] }) }
                  title="+"
                />
              </Row>
              <Row>
                <p>
                  List breeds of your pets:
                  <span className="required-star">*</span>
                </p>
              </Row>
              <Row>
                { user.petBreeds.map((name, index) => (
                  <TextInput
                    placeholder="Bulldog"
                    value={ name }
                    key={ index }
                    handleChange={ (e) => {
                      user.petBreeds[ index ] = e.target.value
                      setUser( {
                        ...user,
                        petBreeds: user.petBreeds,
                      } )
                    } }
                    type="text"
                    lgColSize={ 2 }
                    mdColSize={ 2 }
                    smColSize={ 4 }
                    xsColSize={ 4 }
                  />
                ))}
                <DefaultButton
                  className=""
                  handleClick={ () => setUser({ ...user, petBreeds: [ ...user.petBreeds, '' ] }) }
                  title="+"
                />
              </Row>
            </div>
          )}
          <h1>CURRENT VETERINARIAN</h1>
          <Row>
            <TextInput
              text="Name of Veterinarian Office:"
              placeholder="Happy Paws, Inc"
              id="veterinarianName"
              value={ user.veterinarianName }
              handleChange={ e => setUser( { ...user, veterinarianName: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Phone Number of Veterinarian Office:"
              placeholder="555-123-4567"
              id="veterinarianPhone"
              value={ user.veterinarianPhone }
              handleChange={ e => setUser( { ...user, veterinarianPhone: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <h1>FOSTER CARE QUESTIONS</h1>
          <Row>
            <TextInput
              text="Where will your foster be kept during the day?"
              placeholder=""
              id="fosterDayLocation"
              value={ user.fosterDayLocation }
              handleChange={ e => setUser( { ...user, fosterDayLocation: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Where will your foster sleep at night?"
              placeholder=""
              id="fosterNightLocation"
              value={ user.fosterNightLocation }
              handleChange={ e => setUser( { ...user, fosterNightLocation: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="How many hours will your foster be left alone each day?"
              placeholder="8"
              id="fosterLeftAloneHrs"
              value={ user.fosterLeftAloneHrs }
              handleChange={ e => setUser( { ...user, fosterLeftAloneHrs: e.target.value } ) }
              type="number"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Where will your foster be kept while you are not at home?"
              placeholder="Guest Bedroom"
              id="fosterLeftAloneLocation"
              value={ user.fosterLeftAloneLocation }
              handleChange={ e => setUser( { ...user, fosterLeftAloneLocation: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="How will your foster be exercised?"
              placeholder=""
              id="fosterExercise"
              value={ user.fosterExercise }
              handleChange={ e => setUser( { ...user, fosterExercise: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <p>
              Do you have a fenced yard?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="fencedYard"
              id="fencedYard"
              onChange={ e => setUser( {
                ...user,
                fencedYard: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          { user.fencedYard === 'Yes' && (
            <div>
              <Row>
                <TextInput
                  text="Type of fence:"
                  placeholder=""
                  id="fenceType"
                  value={ user.fenceType }
                  handleChange={ e => setUser( {
                    ...user,
                    fenceType: e.target.value,
                  } ) }
                  type="text"
                  lgColSize={ 8 }
                  mdColSize={ 8 }
                  smColSize={ 12 }
                  xsColSize={ 12 }
                  required
                />
              </Row>
              <Row>
                <TextInput
                  text="Height of fence:"
                  placeholder=""
                  id="fenceHeight"
                  value={ user.fenceHeight }
                  handleChange={ e => setUser( {
                    ...user,
                    fenceHeight: e.target.value,
                  } ) }
                  type="text"
                  lgColSize={ 8 }
                  mdColSize={ 8 }
                  smColSize={ 12 }
                  xsColSize={ 12 }
                  required
                />
              </Row>
            </div>
          )}
          <Row>
            <p>
              Do you have a dog door?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="dogDoor"
              id="dogDoor"
              onChange={ e => setUser( {
                ...user,
                dogDoor: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Can you provide your own kennel?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="ownKennel"
              id="ownKennel"
              onChange={ e => setUser( {
                ...user,
                ownKennel: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Can you provide food?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="canProvideFood"
              id="canProvideFood"
              onChange={ e => setUser( {
                ...user,
                canProvideFood: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Are you willing to administer medication if needed?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="canAdministerMedication"
              id="canAdministerMedication"
              onChange={ e => setUser( {
                ...user,
                canAdministerMedication: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Please check the type of dogs you would consider fostering:
              <span className="required-star">*</span>
            </p>
          </Row>
          <Row>
            { user.dogTypes.map((type, index) => (
              <Label
                lg={ 6 }
                md={ 6 }
                sm={ 12 }
                xs={ 12 }
                key={ type.name }
              >
                <Input
                  type="checkbox"
                  name={ type.name }
                  id={ type.name }
                  onChange={ (e) => {
                    user.dogTypes[ index ] = {
                      ...user.dogTypes[ index ],
                      checked: e.target.checked,
                    }
                    setUser({
                      ...user,
                      dogTypes: user.dogTypes,
                    })
                  } }
                />
                { type.name }
              </Label>
            ))}
          </Row>
          <Row>
            <TextInput
              text="Please list any questions or concerns regarding fostering that we can answer for you:"
              placeholder=""
              id="fosterQuestionsConcerns"
              value={ user.fosterQuestionsConcerns }
              handleChange={ e => setUser( { ...user, fosterQuestionsConcerns: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <TextInput
              text="Explain your reason for wanting to foster an animal at this time:"
              placeholder=""
              id="fosterReason"
              value={ user.fosterReason }
              handleChange={ e => setUser( { ...user, fosterReason: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <Row>
            <p>
              Are you willing to have a Foster Representative visit your home?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="canFosterRepresentativeVisit"
              id="canFosterRepresentativeVisit"
              onChange={ e => setUser( {
                ...user,
                canFosterRepresentativeVisit: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Can you transport your fosters to Veterinarians for medical attention if needed?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="canTransportToVet"
              id="canTransportToVet"
              onChange={ e => setUser( {
                ...user,
                canTransportToVet: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Can you transport your fosters to Spay Today for neuter/spay appointments? (drop off is 7:00am, pickup 4:00pm - M,T,W,Sun)
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="canTransportToNeuterSpay"
              id="canTransportToNeuterSpay"
              onChange={ e => setUser( {
                ...user,
                canTransportToNeuterSpay: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Do you have a warm, dry ventilated room for the foster pet to be quarantined?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="haveWarmDryVentilated"
              id="haveWarmDryVentilated"
              onChange={ e => setUser( {
                ...user,
                haveWarmDryVentilated: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Are you able/willing to bring the dog to adoption events?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="canBringToAdoptionEvents"
              id="canBringToAdoptionEvents"
              onChange={ e => setUser( {
                ...user,
                canBringToAdoptionEvents: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <p>
              Are you able/willing to have potential adopters visit your home?
              <span className="required-star">*</span>
            </p>
            <Input
              type="select"
              name="canHostPotentialAdopters"
              id="canHostPotentialAdopters"
              onChange={ e => setUser( {
                ...user,
                canHostPotentialAdopters: e.target.value,
              } ) }
            >
              <option>No</option>
              <option>Yes</option>
            </Input>
          </Row>
          <Row>
            <TextInput
              text="What is your experience with training a dog? (potty, leash, barking, basic commands, etc)"
              placeholder=""
              id="trainingExperience"
              value={ user.trainingExperience }
              handleChange={ e => setUser( { ...user, trainingExperience: e.target.value } ) }
              type="text"
              lgColSize={ 8 }
              mdColSize={ 8 }
              smColSize={ 12 }
              xsColSize={ 12 }
              required
            />
          </Row>
          <h1>REFERENCES</h1>
          <p>Please list the names and phone numbers for 3 references who routinely see you with your pets, do not live in your home and are not related to you.  Example: neighbor, friends, pet sitters, etc.</p>
          { user.references.map((reference, index) => (
            <div key={ index }>
              <Row>
                <TextInput
                  text="Reference Name"
                  placeholder=""
                  id={ `referenceName${ index + 1 }` }
                  value={ reference.name }
                  handleChange={ (e) => {
                    const tempRefs = [ ...user.references ]
                    tempRefs[ index ].name = e.target.value
                    setUser({ ...user, references: tempRefs })
                  } }
                  type="text"
                  lgColSize={ 8 }
                  mdColSize={ 8 }
                  smColSize={ 12 }
                  xsColSize={ 12 }
                  required
                />
              </Row>
              <Row>
                <TextInput
                  text="Reference Phone"
                  placeholder=""
                  id={ `referencePhone${ index + 1 }` }
                  value={ reference.phone }
                  handleChange={ (e) => {
                    const tempRefs = [ ...user.references ]
                    tempRefs[ index ].phone = e.target.value
                    setUser({ ...user, references: tempRefs })
                  } }
                  type="text"
                  lgColSize={ 8 }
                  mdColSize={ 8 }
                  smColSize={ 12 }
                  xsColSize={ 12 }
                  required
                />
              </Row>
            </div>
          ))}
          <p>Should you be selected as a foster parent and accept a foster dog into your care, you will be required to agree to our Foster Agreement.  The Foster Agreement states that you are a representative of Low Riders of the West and that you have responsibility for the dog while in your care.  The agreement also states that you and your family agree that Low Riders of the West will not be held responsible for damage or injury caused by foster dogs.  As a Low Rider's foster parent you are expected to follow any and all policies of Low Riders of the West.  You will be given information regarding vaccination schedules, medications, and special diet and/or feeding instructions as well as general health care for the particular dog you will be fostering.</p>
          <Row>
            <p>
              BY CLICKING THIS BOX YOU ARE ELECTRONICALLY SIGNING THIS AGREEMENT
              <span className="required-star">*</span>
            </p>
            <Input
              type="checkbox"
              name="agreementSigned"
              id="agreementSigned"
              onChange={ e =>
                setUser({
                  ...user,
                  agreementSigned: e.target.checked,
                })
              }
            />
          </Row>
          <Row>
            <Input
              type="submit"
              id="submit-application"
              value="Submit"
            />
          </Row>
        </Form>
      </Container>
    </div>
  )
}

export default FosterApplication
