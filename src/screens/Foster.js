import React, { useState } from 'react'
import { DefaultButton } from '../components/formComponents'
import FosterInformation from './FosterInformation'
import FosterApplication from './FosterApplication'

const Foster = () => {
  const [ showApplication, setShowApplication ] = useState(false)
  return (
    <div>
      <FosterInformation />
      <DefaultButton
        title="Apply Today"
        styling=""
        handleClick={ () => setShowApplication(!showApplication) }
      />
      { showApplication && <FosterApplication /> }
    </div>
  )
}

export default Foster
