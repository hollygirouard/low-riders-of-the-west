import React from 'react'
import { Container } from 'reactstrap'
import { TextBlock } from '../components'

const TestComponentScreen = () => ( <Container> <TextBlock /> </Container>)

export default TestComponentScreen
