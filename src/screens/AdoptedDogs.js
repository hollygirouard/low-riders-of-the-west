import React from 'react'
import { Container } from 'reactstrap'

const AdoptedDogs = () => (
  <div>
    <Container>Adopted Dogs</Container>
  </div>
)

export default AdoptedDogs
