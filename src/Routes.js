import React from 'react'
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'
import Home from './screens/Home'
import Adopt from './screens/Adopt'
import Contracts from './screens/Contracts'
import Foster from './screens/Foster'
import Events from './screens/UpcomingEvents'
import WaysToHelp from './screens/WaysToHelp'
import SingleDog from './screens/SingleDog'
import TestComponentScreen from './screens/TestComponentScreen'

export default () => (
  <Router>
    <Switch>
      <Route path="/" exact component={ Home } />
      <Route path="/adopt" component={ Adopt } />
      <Route path="/contracts" component={ Contracts } />
      <Route path="/foster" component={ Foster } />
      <Route path="/events" component={ Events } />
      <Route path="/ways-to-help" component={ WaysToHelp } />
      <Route path="/single" component={ SingleDog } />
      <Route path="/test" component={ TestComponentScreen } />
    </Switch>
  </Router>
)
