import BissellPartner from './BissellParnersForPets.png'
import DogIsMyCoPilot from './DIMC_Logo.png'
import LostPetUSA from './LostPetUSA.png'
import AnimalTransportDC from './AnimalTransportDC.jpg'
import Logo from './logo.png'
import NewLogo from './low-riders-logo.png'
import NewLogoLarge from './low-riders-logo@2x.png'
import TacoDog from './Taco.JPG'

export {
  BissellPartner, DogIsMyCoPilot, LostPetUSA, AnimalTransportDC, Logo,
  NewLogo, NewLogoLarge, TacoDog,
}
