import React, { useState } from 'react'
import { Container, Card, CardImg, Col, Row, CardDeck, } from 'reactstrap'
import { BissellPartner, DogIsMyCoPilot, LostPetUSA, AnimalTransportDC } from '../assets'

const Partners = () => {
  const [ partnerLogos, setPartnerLogos ] = useState([ BissellPartner,DogIsMyCoPilot,LostPetUSA,AnimalTransportDC])

  return (
    <div className="home-partners">
      <h1>Our Partners</h1>
      <Container>
        <Row>
          { partnerLogos.map(logo => (
            <Col className="align-self-center">
              <Card>
                <CardImg src={ logo } />
              </Card>
            </Col>
          ))
          }
        </Row>
      </Container>
    </div>
  )
}

export default Partners
