import React from 'react'
import { Card, CardBody, Col } from 'reactstrap'
import { DefaultButton } from './formComponents'
import { DefaultCardTitle, DefaultCardSubtitle, DefaultCardText, } from './cardComponents'

const DogCard = ({ name, age, sex, breed, description, img, btnText, adopted, onClick }) => {

  return (
      <Col sm={ 10 } md={ 8 } lg={ 4 }>
        <Card className="card">
          <div className="card-img-container-angled">
            <img src={ img } alt={`${ name }/${ breed }`} top="true" className="card-img" />
          </div>
          <CardBody className="card-body center">
            <div className="card-title-container">
              <DefaultCardTitle styling="card-title" title={ name } />
              <div className="underline"></div>
            </div>
            <DefaultCardSubtitle styling="card-subtitle" age={ age } sex={ sex } breed={ breed } />
            <DefaultCardText styling="card-text main-text" text={ description } />
            <DefaultButton
              title={ btnText }
              styling={ adopted === true ?  "coral-button adopted" : "coral-button"}
              handleClick={ onClick}
              disableMe={ adopted === true && true}
            />
          </CardBody>
        </Card>
      </Col>
  )
}
export default DogCard
