import React from 'react'
import { Container, Col } from 'reactstrap'
import ReusableCarousel from './ReusableCarousel'

const TopHomeContainer = () => {
  return (
    <div className="top-home-container center vertical-center">
      <ReusableCarousel />
      <Container className="center">
        <Col lg={ 8 } md={ 8 } className="top-home-subtitle">
          <h2>
            We are a small dog rescue specializing in Low Riders and the occasional Chihuahua.
          </h2>
        </Col>
      </Container>
    </div>
  )
}

export default TopHomeContainer
