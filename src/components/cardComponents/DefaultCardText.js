import React from 'react'
import { CardText } from 'reactstrap'

const DefaultCardText = ({ text, styling }) => (
  <CardText className={styling}>
    {text}
  </CardText>
)

export default DefaultCardText