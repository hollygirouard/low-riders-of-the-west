import React from 'react'
import { CardSubtitle } from 'reactstrap'

const DefaultCardSubtitle = ({ age, sex, breed, styling }) => (
  <CardSubtitle className={ styling }>
    { age }  <span>&#x25CF;</span>  {sex }  { breed }
  </CardSubtitle>
)

export default DefaultCardSubtitle
