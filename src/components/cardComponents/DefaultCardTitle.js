import React from 'react'
import { CardTitle } from 'reactstrap'

const DefaultCardTitle = ({ title, styling }) => (
  <CardTitle className={ styling }>
    {title}
  </CardTitle>
)

export default DefaultCardTitle
