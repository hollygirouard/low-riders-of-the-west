import React, { useState } from 'react'
import { Container, Row } from 'reactstrap'
import { TextInput, DefaultButton } from './formComponents'

const ContactUs = () => {
  const [ name, setName ] = useState('')
  const [ email, setEmail ] = useState('')
  const [ message, setMessage ] = useState('')

  return (
    <div className="contact-us">
      <h1>Contact Us</h1>
      <Container className="input-rows">
        <Row className="center-input">
          <TextInput
            text="Name"
            placeholder="Full Name"
            id="name"
            value={ name }
            handleChange={ e => setName( e.target.value ) }
            type="text"
            lgColSize={ 8 }
            mdColSize={ 8 }
            smColSize={ 12 }
            xsColSize={ 12 }
          />
        </Row>
        <Row className="center-input">
          <TextInput
            text="Email"
            placeholder="Email"
            id="email"
            value={ email }
            handleChange={ e => setEmail( e.target.value ) }
            type="email"
            lgColSize={ 8 }
            mdColSize={ 8 }
            smColSize={ 12 }
            xsColSize={ 12 }
          />
        </Row>
        <Row className="center-input">
          <TextInput
            text="Message"
            placeholder="Message"
            id="message"
            value={ message }
            handleChange={ e => setMessage( e.target.value ) }
            type="textarea"
            lgColSize={ 8 }
            mdColSize={ 8 }
            smColSize={ 12 }
            xsColSize={ 12 }
          />
        </Row>
        <Row>
          <DefaultButton title="Submit" styling="green-button" />
        </Row>
      </Container>
    </div>
  )
}

export default ContactUs
