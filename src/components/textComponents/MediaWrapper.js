import React from 'react'
import { Media, Col } from 'reactstrap'

/**
 * Parses string into discrete paragraph elements
 * @param {string} text
 */

function parseBody( text ) {
  const textArray = text.split('\n')
  return textArray.map(paragraph => <><p>{ paragraph }</p></>)
}

/**
 * @param  {string} textBody
 * @param  {string} headingText
 * @param  {string} iconPath
 * @param  {string|data} imgSrc
 * @param  {string} altText
 */

function MediaWrapper( {
  textBody, headingText, iconPath, imgSrc, altText,
} ) {
  const textArray = parseBody( textBody )

  return (
    <Media>
      <Media left href={ iconPath }>
        <Media object src={ imgSrc } alt={ altText } height="auto" width="100" />
      </Media>
      <Media body>
        <Media heading>
          <h3>{headingText}</h3>
        </Media>
        <Col>{ textArray[ 0 ]}</Col>
      </Media>
    </Media>
  )
}

export default MediaWrapper
