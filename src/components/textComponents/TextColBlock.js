import React from 'react'
import { Col, Row } from 'reactstrap'

/**
 * Parses string into discrete paragraph elements
 * @param  {string} text
 *
 */

function parseBody( text ) {
  const textArray = text.split('\n')
  return textArray.map(paragraph => <><p>{ paragraph }</p></>)
}

/**
 * @param  {string} textBody
 */

const TextColBlock = ({ textBody }) =>
  (
    <Row>
      <Col
        sm={ { size: 10, offset: 1 } }
        md={ { size: 8, offset: 2 } }
        lg={ { size: 8, offset: 2 } }
        xl={ { size: 8, offset: 2 } }
      >
        <p>{ parseBody(textBody) }</p>
      </Col>
    </Row>
  )

export default TextColBlock
