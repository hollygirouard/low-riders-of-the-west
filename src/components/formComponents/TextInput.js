import React from 'react'
import { Label, Col, FormGroup, Input, FormFeedback } from 'reactstrap'

const TextInput = ({
  xsColSize, smColSize, mdColSize, lgColSize,
  styling, labelSize, icon, text, required,
  customInput, type, value, id, name,
  placeholder, disabled, feedbackText,
  handleChange,
}) => (
  <Col
    xs={ xsColSize }
    sm={ smColSize }
    md={ mdColSize }
    lg={ lgColSize }
    className={ styling }
  >
    <Label sm={ labelSize } className="input-label">
      {/* { icon ?
        <FontAwesomeIcon icon={ icon } />
            : null } */}
      { text } { required && <span className="required-star">*</span> }
    </Label>
    <FormGroup>
      { customInput ||
      <Input
        type={ type }
        style={ type === 'textarea' ? { height: '300%' } : null }
        value={ value }
        id={ id }
        name={ name }
        onChange={ (e) => {
                  handleChange(e)
                } }
        // valid={ validate.nameState === 'has-success' }
        // invalid={ invalid || (validate.nameState === 'has-danger') }
        placeholder={ placeholder }
        disabled={ disabled }
        // ref={ parent ? (c) => {
        //   parent.trackedInput = c
        //   } : null }
      />
            }
      <FormFeedback>
        { feedbackText && `Please enter ${ feedbackText }.` }
      </FormFeedback>
    </FormGroup>
  </Col>
)

export default TextInput
