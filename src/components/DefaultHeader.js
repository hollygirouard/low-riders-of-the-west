import React from 'react'
import { Logo } from '../assets'

const DefaultHeader = ({ title, subtitle }) => (
  <div className="header center">
    <div className="default-banner">
      <img
        src={ Logo }
        alt="Default Logo"
        width={ 300 }
        max-width="90%"
      />
      <div className="default-banner-title">{ title }</div>
      <div className="default-banner-subtitle">{ subtitle }</div>
    </div>
  </div>
)

export default DefaultHeader
