import React from 'react'
import { Container } from 'reactstrap'

const AboutUs = () => (
  <div className="about-us">
    <h1>About Us</h1>
    <Container className="main-text">
      Low Riders of the West is a nonprofit 501 (c)(3) volunteer based dog rescue.
       We rescue the unwanted, thrown out and abandoned dogs who are at a high risk
        of being euthanized.
        <br /><br />
        We’re dedicated to rescuing small dogs from high kill
         shelters throughout the Southwest and finding new homes for them. We work
          with veterinarians, trainers, fosters and and dog lovers in our community to
           provide a safe and loving environment for each dog to decompress and rehabilitate
            in before finding their furever home.
            <br /><br />
            All of our rescues are vaccinated,
             given medical attention, spayed/ neutered (if over 6 months of age) and
              chipped before they are adopted out. Low Riders relies completely on volunteer
               foster homes for our dogs to stay in while they are waiting to be adopted.
    </Container>
  </div>
)

export default AboutUs
