import React from 'react'
import DogCard from './DogCard'
import { DefaultButton } from '../components/formComponents'
import { doglist } from '../helpers/sampledata'

const AdoptableDogs = ({ limit }) => {
  if (limit) { // randomize the doggo list if we're only showing a few (like on the homepage)
    for (let i = (doglist.length - 1); i > 0; i--) {
      const j = Math.floor(Math.random() * i)
      const temp = doglist[ i ]
      doglist[ i ] = doglist[ j ]
      doglist[ j ] = temp
    }
  } else {
    // for the main page, remove the adopted dogs
    doglist.filter(dog => !dog.status)
  }

  const dogs = (limit) ? doglist.slice(0, limit) : doglist

  const seeDog = () => {
    console.log('hi')
  }

  return (
    <div className="adoptable-dogs center">
      <h1> Adoptable Dogs </h1>
      <div className="card-deck center">
        <>
          {
            dogs.map((ele, i) => (
              <DogCard
                key={ `${ ele.name }${ i }` }
                name={ ele.name }
                age={ ele.age }
                breed={ ele.breed }
                sex={ ele.sex }
                img={ ele.img }
                description={ ele.description }
                adopted={ ele.status }
                onClick={ seeDog }
                btnText={ ele.status === true ? 'Adopted!' : 'Read More' }
              />

                // navTo prop still needed in order to navigate with button
              ))
          }
        </>
      </div>
      <DefaultButton
        title="See More Adoptable Dogs"
        styling="green-button center"
      />
    </div>
  )
}

export default AdoptableDogs
