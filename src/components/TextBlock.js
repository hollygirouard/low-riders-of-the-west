import React from 'react'
import { MediaWrapper } from './textComponents'
import loremIpsumSample from './textComponents/loremIpsumSample'

/**
 * mediaWrapper accepts props: textBody, headingText, iconPath, imgSrc, altText
 */

function TextBlock() {
  return (
    <>
      <MediaWrapper
        textBody={ loremIpsumSample() }
        headingText="This is a sample header"
        iconPath="/"
        imgSrc=""
        altText=""
        className="text-block"
      />
      {/* <TextColBlock textBody={ loremIpsumSample() } /> */}
    </>
  )
}

export default TextBlock
