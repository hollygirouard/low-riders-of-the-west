require('dotenv').config()

const express = require('express')
const app = express()
const dogRouter = require('./api/dogRoutes')
const createRouter = require('./api/createTestRoutes')
const apiRouter = require('./api/api')


const bodyParser = require('body-parser')

const dev = process.env.NODE_ENV && process.env.NODE_ENV !== 'production'

const port = dev ? 8081 : 8080

/////////////
//Middleware
////////////

app.use(express.json())
app.use(bodyParser.urlencoded({
  extended: true
}));

/////////
// Routes
/////////

app.use('/dogs', dogRouter)
app.use('/', createRouter)
app.use('/', apiRouter)


app.listen(port, () => {
  console.log('Low Riders of the West listening on port', port)
})