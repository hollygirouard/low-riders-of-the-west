// Set up express router
const express = require('express');
const sql = require('./../db/dog')

const router = express.Router()

router.get('/', async (req, res, next) => {

  try {
    let results = await sql.all()
    res.json(results)
  } catch (err) {
    res.sendStatus(500)
    console.error(err)
  }

})

router.get('/:id', async (req, res, next) => {

  try {
    const results = await sql.findOne(req.params.id)
    const json = await res.json(results)
    res.send(json)
  } catch (err) {
    res.sendStatus(500)
    console.error(err)
  }
})

router.post('/', async (req, res, next) => {
  console.log(req.body);
  try {
    const results = await sql.createDog(req.body)
    const json = await res.json(results)
    res.send(json)

  } catch (err) {
    res.send(500)
    console.error(err);
  }

});



module.exports = router