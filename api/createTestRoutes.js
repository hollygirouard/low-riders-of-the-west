const express = require('express')
const sql = require('./../db/create')
const router = express.Router()

// /createdb, then add db name to database in db.js, then /createtables then /seed

router.get('/createdb', async (req, res) => {
  const result = await sql.createDb()
  res.send(result)

});

router.get('/createtables', async (req, res) => {
  const result = await sql.createtables()
  res.send(result)
});

router.get('/seed', async (req, res) => {
  const result = await sql.seedTables()
  res.send(result)
});


module.exports = router